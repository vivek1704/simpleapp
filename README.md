# SimpleApp
- created a simple program in java for printing the factorial of 5.

## Add your files
```
git init
git add .
git commit -m "initial"
git remote add origin https://gitlab.com/vivek1704/simpleapp.git
git push -uf origin master
```

## Steps for creating and checking status of gitlab CI/CD pipelines
- After above step merge master branch into main branch.
- create .gitlab-ci.yaml file into main branch and push it into your GitLab repository.
- Once pushed, GitLab CI/CD will automatically detect the pipeline. Check the pipeline into build section on your repository to see the pipeline progress.

## Steps for triggering CI pipeline into jenkins
- create Jenkinsfile into your repository and define all the stages.
- now goto into setting of specific repository and then select the integrations.
- select jenkins into menu and configure to jenkins.
- at the time of configuration, you need to provide the details like jenkins sever link, username, password, project name and also you need to check the boxes when you want to trigger 
- save it and whenever new commit will be pushed into the repository, in jenkins automatically build job will be started.

## Troubleshooting
- if GitLab pipeline fails, review the CI/CD logs for error messages. Ensure that your appication is correctly configured. Check if the java appication is compiled successfully or not.

- if integration with jenkins fails, review the configuration into integrations and ensure that sever link is correct and it should be accessible from the browser.



